import argparse
import time
from enum import Enum
import math
import numpy as np

from udacidrone import Drone
from udacidrone.connection import MavlinkConnection, WebSocketConnection  # noqa: F401
from udacidrone.messaging import MsgID


class States(Enum):
    MANUAL = 0
    ARMING = 1
    TAKEOFF = 2
    WAYPOINT = 3
    LANDING = 4
    DISARMING = 5


class BackyardFlyer(Drone):

    def __init__(self, connection):
        super().__init__(connection)
        self.target_position = np.array([0.0, 0.0, 0.0])
        self.all_waypoints = []
        self.in_mission = True
        self.check_state = {}
        self.target_altitude = -3.0
        self.box_length = 10.0
        self.error_margin = 0.05 * self.box_length
        # initial state
        self.flight_state = States.MANUAL

        # TODO: Register all your callbacks here
        self.register_callback(MsgID.LOCAL_POSITION, self.local_position_callback)
        self.register_callback(MsgID.LOCAL_VELOCITY, self.velocity_callback)
        self.register_callback(MsgID.STATE, self.state_callback)

    def local_position_callback(self):
        self.velocity_or_pos_change()
        pass

    def velocity_callback(self):
        self.velocity_or_pos_change()
        pass

    def velocity_or_pos_change(self):
        if self.flight_state == States.WAYPOINT or self.flight_state == States.TAKEOFF:
            vel_magnitude = math.sqrt(np.sum(self.local_velocity**2))            
            if self.check_postion_distance(self.target_position, self.local_position) < self.error_margin and vel_magnitude < 1:
                print ("distance and velocity before changing direction", self.target_position, self.local_position, vel_magnitude)           
                if len(self.all_waypoints) > 0:
                    self.target_position = self.all_waypoints[0]
                    self.all_waypoints = self.all_waypoints[1:]
                    self.waypoint_transition()
                else:
                    self.landing_transition()
        if self.flight_state == States.LANDING:
            if ((self.global_position[2] - self.global_home[2] < 0.1) and abs(self.local_position[2]) < 0.1):
                print ("distance between home and global", math.sqrt(np.sum(self.local_position**2)))           
                self.disarming_transition()

    def check_postion_distance(self, position1, position2):
        position_vector = position1 - position2
        ssq = np.sum(position_vector**2)
        return math.sqrt(ssq)


    def state_callback(self):
        """
        TODO: Implement this method

        This triggers when `MsgID.STATE` is received and self.armed and self.guided contain new data
        """
        if not self.in_mission:
            return
        if self.flight_state == States.MANUAL:
            self.arming_transition()
        elif self.flight_state == States.ARMING:
            if self.armed:
                self.takeoff_transition()
        elif self.flight_state == States.DISARMING:
            if not self.armed:
                self.manual_transition()
        pass

    def calculate_box(self):
        self.all_waypoints.append([self.box_length, 0, self.target_altitude])
        self.all_waypoints.append([self.box_length, self.box_length, self.target_altitude])
        self.all_waypoints.append([0, self.box_length, self.target_altitude])
        self.all_waypoints.append([0, 0, self.target_altitude])
        pass

    def arming_transition(self):
        print("arming transition")
        self.take_control()
        self.arm()

        # set the current location to be the home position
        self.set_home_position(self.global_position[0],
                               self.global_position[1],
                               self.global_position[2])
        self.calculate_box()
        self.flight_state = States.ARMING

    def takeoff_transition(self):
        print("takeoff transition")
        self.target_position[2] = self.target_altitude
        # To negate the GPS pos
        self.takeoff(self.target_altitude*-1)
        self.flight_state = States.TAKEOFF

    def waypoint_transition(self):
        self.cmd_position(self.target_position[0], self.target_position[1], self.target_position[2]*-1, 0)
        self.flight_state = States.WAYPOINT
        print("waypoint transition")

    def landing_transition(self):
        print("landing transition")
        self.target_position[2] = 0
        self.land()
        self.flight_state = States.LANDING

    def disarming_transition(self):
        print("disarm transition")
        self.disarm()
        self.flight_state = States.DISARMING

    def manual_transition(self):
        print("manual transition")

        self.release_control()
        self.stop()
        self.in_mission = False
        self.flight_state = States.MANUAL

    def start(self):
        """This method is provided
        
        1. Open a log file
        2. Start the drone connection
        3. Close the log file
        """
        print("Creating log file")
        self.start_log("Logs", "NavLog.txt")
        print("starting connection")
        self.connection.start()
        print("Closing log file")
        self.stop_log()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--port', type=int, default=5760, help='Port number')
    parser.add_argument('--host', type=str, default='127.0.0.1', help="host address, i.e. '127.0.0.1'")
    args = parser.parse_args()

    conn = MavlinkConnection('tcp:{0}:{1}'.format(args.host, args.port), threaded=False, PX4=False)
    #conn = WebSocketConnection('ws://{0}:{1}'.format(args.host, args.port))
    drone = BackyardFlyer(conn)
    time.sleep(2)
    drone.start()
